import { forwardRef, INestApplication } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Test, TestingModule } from "@nestjs/testing";
import * as request from "supertest";
import { AppController } from "../src/app.controller";
import { Todo, TodoSchema } from "../src/todos/schemas/todo.schema";
import { TodosController } from "../src/todos/todos.controller";
import { rootMongooseTestModule } from "../src/utils/test/mongo/MongooseTestModule";
import API_MESSAGE from "../src/utils/apiMessage";

describe("AppController (e2e)", () => {
    let app: INestApplication;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [],
            controllers: [AppController],
            providers: [],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it("/ (GET)", () => {
        return request(app.getHttpServer()).get("/").expect(200).expect(API_MESSAGE.WELCOME);
    });
});

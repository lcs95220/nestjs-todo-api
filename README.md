# API TODO - NestJs

Un exemple d'application type Todo implémentée avec NestJs.

Les données sont persistées dans un container docker mongodb.
L'application peut se lancer de manière classique avec npm et également via docker.

Des tests automatisés sont rédigés avec Jest.

## Technologies utilisées

![](https://img.shields.io/badge/-Node.js-339933?style=for-the-badge&logo=Node.js&logoColor=fff)
![](https://img.shields.io/badge/-NPM-CB3837?style=for-the-badge&logo=NPM&logoColor=fff)
![](https://img.shields.io/badge/-TypeScript-007ACC?style=for-the-badge&logo=TypeScript&logoColor=fff)
![](https://img.shields.io/badge/-NestJs-F8F8F5?style=for-the-badge)
![](https://img.shields.io/badge/-Nodemon-76D04B?style=for-the-badge&logo=Nodemon&logoColor=fff)
![](https://img.shields.io/badge/-ESLint-4B32C3?style=for-the-badge&logo=ESLint&logoColor=fff)
![](https://img.shields.io/badge/-Prettier-F7B93E?style=for-the-badge&logo=Prettier&logoColor=000)
![](https://img.shields.io/badge/-Jest-C21325?style=for-the-badge&logo=Jest&logoColor=fff)
![](https://img.shields.io/badge/-Swagger-85EA2D?style=for-the-badge&logo=Swagger&logoColor=000)
![](https://img.shields.io/badge/-Docker-2496ED?style=for-the-badge&logo=Docker&logoColor=fff)
![](https://img.shields.io/badge/-MongoDB-47A248?style=for-the-badge&logo=MongoDB&logoColor=fff)

Icones provenant de [simpleicons](https://simpleicons.org/)

# Installation

A la racine du projet :

-   `npm install`
-   Modifier les variables d'environnement dans le fichier _.env.example_ et renommer _.env_

## Lancement de l'application :

-   ### Sans docker :

    -   Il faut au préalable avoir mongodb d'installé et lancer le serveur
    -   Modifier les variables d'environnement si besoin
    -   ` npm run dev` # environnement de développement avec redémarrage automatique
    -   `npm start` # environnement de développement sans redémarrage automatique
    -   ` npm start:prod` # environnement de production

<br>

-   ### Avec docker :

    Dans le fichier _docker-compose.yml_, modifier les variables d'environnement du container mongo de manière a ce qu'elles aient les mêmes valeurs que le fichier _.env_ du projet.

    1. Environnement de développement

        - `docker-compose up`

    2. Environnement de production

        - Dans le fichier _docker-compose.yml_, modifier la valeur **Dockerfile.dev** par **Dockerfile**
        - `docker-compose up`

## Tests

Pour lancer les tests unitaires :
` npm run test`

Pour les tests en mode debug :
`npm run test:debug`

Pour les tests end to end :
`npm run test:e2e`

## Documentation

Une documentation Swagger rédigée grâce aux décorateurs fournis pas NestJs est disponible à l'url _/docs_ pour une version graphique ou _/docs-json_ pour la version json.

## Qualité de code

Eslint et Prettier sont utilisés pour la qualité du code.
Les configurations respectives sont disponibles dans les fichiers _.eslintrc_ et _.prettierrc_.

## Contact

<lucas.hilaire.74@gmail.com>

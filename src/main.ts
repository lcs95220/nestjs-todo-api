import { Logger, ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./app.module";
import * as compression from "compression";
import * as cookieParser from "cookie-parser";
import * as helmet from "helmet";
import * as rateLimit from "express-rate-limit";
import * as dotenv from "dotenv";

class Server {
    public app;

    async bootstrap() {
        const PORT = process.env.PORT || 3000;

        this.app = await NestFactory.create(AppModule, { cors: true, bodyParser: true, logger: true });
        dotenv.config();
        this.initializeSwagger();
        this.initializeMiddleware();
        await this.app.listen(PORT);
        Logger.log(`Serveur lancé sur le port : ${await this.app.getUrl()}`);
    }

    initializeMiddleware() {
        this.app.use(helmet());
        this.app.use(
            rateLimit({
                windowMs: 15 * 60 * 1000,
                max: 100,
            }),
        );
        this.app.use(compression());
        this.app.use(cookieParser());
        this.app.useGlobalPipes(new ValidationPipe());
    }

    initializeSwagger() {
        const config = new DocumentBuilder()
            .setTitle("Todo API")
            .setDescription("Documentation de l'api TodoApp")
            .setVersion("1.0")
            .addTag("users")
            .addTag("todos")
            .addTag("authentication")
            .addBearerAuth({ type: "http", scheme: "bearer", bearerFormat: "JWT" }, "Auth")
            .build();
        const document = SwaggerModule.createDocument(this.app, config);
        SwaggerModule.setup("docs", this.app, document);
    }
}

new Server().bootstrap();

import { MongooseModule } from "@nestjs/mongoose";
import { Test, TestingModule } from "@nestjs/testing";
import * as dotenv from "dotenv";
import { mongo } from "mongoose";
import { closeInMongodConnection, rootMongooseTestModule } from "../utils/test/mongo/MongooseTestModule";
import { CreateUserDto } from "./dto/create-user.dto";
import { User as UserEntity } from "./entities/user.entity";
import { User, UserSchema } from "./schemas/user.schema";
import { UsersService } from "./users.service";

describe("Users Service", () => {
    let usersService: UsersService;
    let userData: CreateUserDto;
    let userResult: UserEntity;

    beforeEach(async () => {
        dotenv.config();
        const module: TestingModule = await Test.createTestingModule({
            imports: [rootMongooseTestModule(), MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])],
            providers: [UsersService],
        }).compile();

        usersService = module.get<UsersService>(UsersService);
        userData = { username: "lucas", password: "root" };
        userResult = await usersService.create(userData);
    });

    it("Service défini", () => {
        expect(usersService).toBeDefined();
    });

    it("Récupération des détails d'un utilisateur", async () => {
        const userDetails: User = await usersService.findOne("lucas");
        expect(userDetails.username).toEqual(userData.username);
        expect(userDetails.password).toEqual(userData.password);
    });

    it("Création d'utilisateur : échec car username déjà existant", async () => {
        try {
            await usersService.create(userData);
        } catch (error) {
            expect(error).toBeDefined();
            expect(error).toBeInstanceOf(mongo.MongoError);
        }
    });

    it("Création d'utilisateur : échec car mot de passe trop court", async () => {
        try {
            const user: CreateUserDto = { username: "test", password: "tst" };
            await usersService.create(user);
        } catch (error) {
            expect(error).toBeDefined();
        }
    });

    it("Création d'utilisateur", async () => {
        try {
            const user: CreateUserDto = { username: "test", password: "test" };
            const result: UserEntity = await usersService.create(user);
            expect(result).toBeDefined();
            expect(result.username).toEqual(user.username);
            expect(result.password).toEqual(user.password);
        } catch {}
    });

    it("Supression d'un utilisateur", async () => {
        await usersService.remove(userResult);
    });

    afterEach(async () => {
        await closeInMongodConnection();
    });
});

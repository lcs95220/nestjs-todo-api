import { ApiProperty } from "@nestjs/swagger";

export class User {
    @ApiProperty({ type: String })
    _id?: string;
    @ApiProperty({ type: String, required: true })
    username: string;
    @ApiProperty({ type: String, required: true })
    password: string;
}

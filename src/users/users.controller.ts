import { BadRequestException, Controller, Delete, Get, Request, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { JwtAuthGuard } from "../auth/guards/jwt-auth.guard";
import { TodosService } from "../todos/todos.service";
import API_MESSAGE from "../utils/apiMessage";
import { User } from "./entities/user.entity";
import { UsersService } from "./users.service";

@ApiTags("users")
@ApiBearerAuth("Auth")
@UseGuards(JwtAuthGuard)
@Controller("users")
export class UsersController {
    constructor(private readonly usersService: UsersService, private readonly todosService: TodosService) {}

    @Get()
    async findOne(@Request() req) {
        const userDetails: User = await this.usersService.findOne(req.user.username);
        const { password, ...userDetailsWithoutPass } = userDetails;
        return userDetailsWithoutPass;
    }

    @Delete()
    async remove(@Request() req) {
        const deleteUser = await this.usersService.remove(req.user);
        if (!deleteUser.deletedCount || deleteUser.deletedCount < 1) throw new BadRequestException(API_MESSAGE.USER_NOT_FOUND);
        await this.todosService.removeAll(req.user);
        return deleteUser;
    }
}

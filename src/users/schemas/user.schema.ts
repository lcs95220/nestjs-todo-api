import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
    @Prop({ required: true, index: true, minlength: 3, unique: true })
    username: string;

    @Prop({ required: true, minlength: 4 })
    password: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

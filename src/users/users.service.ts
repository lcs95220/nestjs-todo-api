import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { CreateUserDto } from "./dto/create-user.dto";
import { Model } from "mongoose";
import { UserDocument } from "./schemas/user.schema";
import { User } from "./entities/user.entity";

@Injectable()
export class UsersService {
    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}
    create(createUserDto: CreateUserDto) {
        return this.userModel.create(createUserDto);
    }

    findOne(username: string): Promise<User> {
        return this.userModel.findOne({ username }).exec();
    }

    remove(user: User) {
        return this.userModel.deleteOne({ _id: user._id });
    }
}

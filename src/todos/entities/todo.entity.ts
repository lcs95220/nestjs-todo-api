import { ApiProperty } from "@nestjs/swagger";

export class Todo {
    @ApiProperty({ type: String })
    _id?: string;
    @ApiProperty({ type: String, required: true })
    name: string;
    @ApiProperty({ type: String, required: true })
    user: string;
    @ApiProperty({ type: String })
    description?: string;
    @ApiProperty({ type: Boolean })
    completed?: boolean;
}

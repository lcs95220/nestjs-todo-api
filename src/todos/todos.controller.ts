import { BadRequestException, Body, Controller, Delete, Get, Param, Post, Put, Req, Request, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { JwtAuthGuard } from "../auth/guards/jwt-auth.guard";
import API_MESSAGE from "../utils/apiMessage";
import { CreateTodoDto } from "./dto/create-todo.dto";
import { UpdateTodoDto } from "./dto/update-todo.dto";
import { Todo } from "./entities/todo.entity";
import { TodosService } from "./todos.service";

@ApiTags("todos")
@ApiBearerAuth("Auth")
@UseGuards(JwtAuthGuard)
@Controller("todos")
export class TodosController {
    constructor(private readonly todosService: TodosService) {}

    @Post()
    async create(@Body() createTodoDto: CreateTodoDto, @Req() req): Promise<Todo> {
        return await this.todosService.create(createTodoDto, req.user);
    }

    @Get()
    async findAll(@Request() req): Promise<Todo[]> {
        return await this.todosService.findAll(req.user);
    }
    @Get(":id")
    async findOne(@Param("id") id: string, @Req() req): Promise<Todo> {
        return await this.todosService.findOne(id, req.user);
    }

    @Put(":id")
    async update(@Param("id") id: string, @Body() updateTodoDto: UpdateTodoDto, @Req() req) {
        const resultUpdate = await this.todosService.update(id, updateTodoDto, req.user);
        if (!resultUpdate.nModified || resultUpdate.nModified < 1) throw new BadRequestException(API_MESSAGE.DATA_NOT_FOUND);
        return await this.todosService.findOne(id, req.user);
    }

    @Delete(":id")
    async remove(@Param("id") id: string, @Req() req) {
        return await this.todosService.remove(id, req.user);
    }
}

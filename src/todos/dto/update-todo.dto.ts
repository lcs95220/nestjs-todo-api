import { IsNotEmpty, IsString, IsBoolean, IsOptional } from "class-validator";

export class UpdateTodoDto {
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    readonly name: string;

    @IsOptional()
    @IsNotEmpty()
    @IsString()
    readonly description?: string;

    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    readonly completed?: boolean;
}

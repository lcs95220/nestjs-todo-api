import { IsNotEmpty, IsString, IsBoolean, IsOptional } from "class-validator";

export class CreateTodoDto {
    @IsNotEmpty()
    @IsString()
    readonly name: string;

    @IsOptional()
    @IsNotEmpty()
    @IsString()
    readonly description?: string;

    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    readonly completed?: boolean;
}

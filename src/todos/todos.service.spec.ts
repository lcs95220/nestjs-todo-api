import { MongooseModule } from "@nestjs/mongoose";
import { Test, TestingModule } from "@nestjs/testing";
import { mongo } from "mongoose";
import { use } from "passport";
import { User } from "src/users/entities/user.entity";
import { closeInMongodConnection, rootMongooseTestModule } from "../utils/test/mongo/MongooseTestModule";
import { CreateTodoDto } from "./dto/create-todo.dto";
import { Todo as TodoEntity } from "./entities/todo.entity";
import { Todo, TodoSchema } from "./schemas/todo.schema";
import { TodosService } from "./todos.service";

describe("TodosService", () => {
    let todosService: TodosService;
    let todoData: CreateTodoDto;
    let todoResult: TodoEntity;
    let user: User;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [rootMongooseTestModule(), MongooseModule.forFeature([{ name: Todo.name, schema: TodoSchema }])],
            providers: [TodosService],
        }).compile();

        todosService = module.get<TodosService>(TodosService);
        user = {
            _id: "60356c5b534c53001f9821de",
            username: "lucas",
            password: "test",
        };
        todoData = {
            name: "Tache test",
            completed: true,
        };
        todoResult = await todosService.create(todoData, user);
    });

    afterEach(async () => {
        await closeInMongodConnection();
    });

    it("Service défini", () => {
        expect(todosService).toBeDefined();
    });

    it("Récupération des détails d'une tâche", async () => {
        const todoDetails: Todo = await todosService.findOne(todoResult._id, user);
        expect(todoDetails.name).toEqual(todoData.name);
        expect(todoDetails.description).not.toBeDefined();
        expect(todoDetails.completed).toBeTruthy();
    });

    it("Création d'une tâche : échec name est vide", async () => {
        try {
            const todo: CreateTodoDto = { name: "" };
            await todosService.create(todo, user);
        } catch (error) {
            expect(error).toBeDefined();
        }
    });

    it("Création d'utilisateur", async () => {
        try {
            const todo: CreateTodoDto = { name: "test" };
            const result: TodoEntity = await todosService.create(todo, user);
            expect(result).toBeDefined();
            expect(result.name).toEqual(todo.name);
        } catch {}
    });

    it("Supression d'une tâche", async () => {
        await todosService.remove(todoResult._id, user);
    });

    afterEach(async () => {
        await closeInMongodConnection();
    });
});

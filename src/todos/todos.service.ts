import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { User } from "../users/entities/user.entity";
import { CreateTodoDto } from "./dto/create-todo.dto";
import { UpdateTodoDto } from "./dto/update-todo.dto";
import { Todo, TodoDocument } from "./schemas/todo.schema";

@Injectable()
export class TodosService {
    constructor(@InjectModel(Todo.name) private todoModel: Model<TodoDocument>) {}

    async create(createTodoDto: CreateTodoDto, user: User): Promise<Todo> {
        return this.todoModel.create({ ...createTodoDto, user: user._id });
    }

    async findAll(user: User): Promise<Todo[]> {
        return await this.todoModel.find({ user: user._id }).exec();
    }

    async findOne(id: string, user: User): Promise<Todo> {
        return await this.todoModel.findOne({ _id: id, user: user._id }).exec();
    }

    async update(id: string, updateTodoDto: UpdateTodoDto, user: User): Promise<any> {
        return await this.todoModel.updateOne({ _id: id, user: user._id }, updateTodoDto).exec();
    }

    async remove(id: string, user: User): Promise<any> {
        return await this.todoModel.deleteOne({ _id: id, user: user._id }).exec();
    }
    async removeAll(user: User) {
        return await this.todoModel.deleteMany({ user: user._id });
    }
}

const API_MESSAGE = {
    USER_NOT_FOUND: "Utilisateur non trouvé",
    USERNAME_NOT_FOUND: "Adresse mail non trouvée",
    USERNAME_ALREADY_EXISTS: "Nom d'utilisateur déjà existant",
    EMPTY_DATA: "Données vides",
    INCORRECT_PASSWORD: "Mot de passe incorrect",
    WRONG_TOKEN: "Token d'authentification incorrect",
    NO_TOKEN: "Aucun token d'authentification fourni",
    GLOBAL_ERROR: "Erreur rencontrée",
    DATA_NOT_FOUND: "Donnée non trouvée",
    DATA_DELETED: "Donnée supprimée",
    DATA_UPDATED: "Donnée modifiée",
    WELCOME: "Bienvenue sur todo API avec NestJs",
};

export default API_MESSAGE;

import { Controller, Get } from "@nestjs/common";
import API_MESSAGE from "./utils/apiMessage";

@Controller()
export class AppController {
    @Get()
    get(): string {
        return API_MESSAGE.WELCOME;
    }
}

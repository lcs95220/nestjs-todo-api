import { IsNotEmpty, IsString, MinLength } from "class-validator";

export class LoginDto {
    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    readonly username: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(4)
    readonly password: string;
}

import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { User } from "../users/entities/user.entity";
import { UsersService } from "../users/users.service";
import * as bcrypt from "bcrypt";

@Injectable()
export class AuthService {
    constructor(private readonly usersService: UsersService, private readonly jwtService: JwtService) {}

    async validateUser(username: string, pass: string): Promise<any> {
        const user: User = await this.usersService.findOne(username);
        if (user && (await bcrypt.compare(pass, user.password))) {
            const result = {
                _id: user._id,
                username: user.username,
            };
            return result;
        }
        return null;
    }

    async login(user: any) {
        const payload = { username: user.username, _id: user._id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}

import { Body, ConflictException, Controller, Post, Request, UseGuards } from "@nestjs/common";
import { ApiBody, ApiTags } from "@nestjs/swagger";
import { CreateUserDto } from "../users/dto/create-user.dto";
import { User } from "../users/entities/user.entity";
import { UsersService } from "../users/users.service";
import { AuthService } from "./auth.service";
import { LoginDto } from "./dto/login.dto";
import { LocalAuthGuard } from "./guards/local-auth.guard";
import * as bcrypt from "bcrypt";
import API_MESSAGE from "../utils/apiMessage";

@ApiTags("authentication")
@Controller("auth")
export class AuthController {
    constructor(private readonly authService: AuthService, private readonly usersService: UsersService) {}
    @UseGuards(LocalAuthGuard)
    @ApiBody({ type: LoginDto })
    @Post("/login")
    login(@Request() req) {
        return this.authService.login(req.user);
    }

    @Post("/signup")
    async signup(@Body() createUserDto: CreateUserDto) {
        const checkUser: User = await this.usersService.findOne(createUserDto.username);
        if (checkUser) throw new ConflictException(API_MESSAGE.USERNAME_ALREADY_EXISTS);
        const userToPersist = await this.getDtoWithHashedPassword(createUserDto);
        this.usersService.create(userToPersist);
        const { password, ...userWithoutPassword } = userToPersist;
        return userWithoutPassword;
    }

    private async getDtoWithHashedPassword(createUserDto: CreateUserDto): Promise<CreateUserDto> {
        const hashedPassword: string = await bcrypt.hash(createUserDto.password, 12);
        const { password, ...userWithoutPass } = createUserDto;
        return { ...userWithoutPass, password: hashedPassword };
    }
}
